package br.org.unirio.controller;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoConsertoBicicleta;
import br.org.unirio.repository.BicicletaRepository;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.service.BicicletaService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import io.javalin.http.Context;
import org.mockito.MockitoAnnotations;

public class BicicletaControllerTest {

//    @Mock
//    private BicicletaService bs;
//    @Mock
//    private BicicletaRepository bikeRepo;
//    @Mock
//    private FuncionarioRepository funcionarioRepo;
//    @Mock
//    private Context ctx;
//    @InjectMocks
//    private BicicletaController bc;
//
//    @Before
//    public void init() {
//        MockitoAnnotations.initMocks(this);
//    }
//
//    @Test
//    public void consertaBicicletaInexistenteTest() throws ObjetoJaExisteException {
//        doReturn(new Bicicleta()).when(bikeRepo).getBicicletaByID(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        when(ctx.queryParam("Bicicleta")).thenReturn("c2dd77a5-c142-42d1-bb05-3df749c69f9b");
//        when(ctx.queryParam("Funcionario")).thenReturn("c2dd77a5-c142-42d1-bb05-3df749c69f9b");
//
//        doNothing().when(bs).criaPedidoConsertoBicicleta(any(UUID.class), any(UUID.class));
//
//        bc.consertaBicicleta(ctx);
//        verify(ctx).status(201);
//    }
//
//    @Test
//    public void consertaBicicletaExistenteTest() throws ObjetoJaExisteException {
//        doReturn(new Bicicleta()).when(bikeRepo).getBicicletaByID(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        when(ctx.queryParam("Bicicleta")).thenReturn("c2dd77a5-c142-42d1-bb05-3df749c69f9b");
//        when(ctx.queryParam("Funcionario")).thenReturn("c2dd77a5-c142-42d1-bb05-3df749c69f9b");
//        doNothing().when(bs).criaPedidoConsertoBicicleta(any(UUID.class), any(UUID.class));
//        doThrow(ObjetoJaExisteException.class).when(bs).criaPedidoConsertoBicicleta(any(UUID.class), any(UUID.class));
//
//        bc.consertaBicicleta(ctx);
//        verify(ctx).status(409);
//    }
//
//    @Test
//    public void consertaBicicletaInvalidoTest() throws ObjetoJaExisteException {
//        doReturn(new Bicicleta()).when(bikeRepo).getBicicletaByID(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        when(ctx.queryParam("Bicicleta")).thenReturn("a");
//        when(ctx.queryParam("Funcionario")).thenReturn("b");
//
//        doThrow(Exception.class).when(bs).criaPedidoConsertoBicicleta(any(UUID.class), any(UUID.class));
//
//        bc.consertaBicicleta(ctx);
//        verify(ctx).status(400);
//    }
}
