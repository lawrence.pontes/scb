package br.org.unirio.service;


import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaBicicleta;
import br.org.unirio.model.PedidoConsertoBicicleta;
import br.org.unirio.repository.BicicletaRepository;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaBicicletaRepository;
import br.org.unirio.repository.PedidoConsertoBicicletaRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BicicletaServiceTest {
    @Mock
    private BicicletaRepository bikeRepo;
    @Mock
    private FuncionarioRepository funcionarioRepo;
    @Mock
    private PedidoConsertoBicicletaRepository pcbRepo;
    @Mock
    private PedidoAposentaBicicletaRepository pabRepo;
    @InjectMocks
    private BicicletaService bs = new BicicletaService();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    public void criaPedidoConsertoBicicletaSucesso(){
//        doReturn(new Bicicleta()).when(bikeRepo).getBicicletaByID(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(pcbRepo).getPedidoConsertoBicicleta(any(PedidoConsertoBicicleta.class));
//        doNothing().when(pcbRepo).adicionaPedidoConsertoBicicleta(any(PedidoConsertoBicicleta.class));
//        try {
//            bs.criaPedidoConsertoBicicleta(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }

//    @Test
//    public void criaPedidoConsertoBicicletaFalha(){
//        when(bikeRepo.getBicicletaByID(any(UUID.class))).thenReturn(new Bicicleta());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(pcbRepo.getPedidoConsertoBicicleta(any(PedidoConsertoBicicleta.class))).thenReturn(new PedidoConsertoBicicleta(new Bicicleta(), new Funcionario()));
//        try {
//            bs.criaPedidoConsertoBicicleta(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeConsertoTest(){
        when(pcbRepo.getPedidosBicicleta()).thenReturn(new ArrayList<PedidoConsertoBicicleta>());

        bs.getPedidosDeConserto();
    }

//    @Test
//    public void criaPedidoAposentaBicicletaSucesso(){
//        doReturn(new Bicicleta()).when(bikeRepo).getBicicletaByID(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(pabRepo).getPedidoAposentaBicicleta(any(PedidoAposentaBicicleta.class));
//        doNothing().when(pabRepo).adicionaPedidoAposentaBicicleta(any(PedidoAposentaBicicleta.class));
//        try {
//            bs.criaPedidoAposentaBicicleta(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }

//    @Test
//    public void criaPedidoAposentaBicicletaFalha(){
//        when(bikeRepo.getBicicletaByID(any(UUID.class))).thenReturn(new Bicicleta());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(pabRepo.getPedidoAposentaBicicleta(any(PedidoAposentaBicicleta.class))).thenReturn(new PedidoAposentaBicicleta(new Bicicleta(), new Funcionario()));
//        try {
//            bs.criaPedidoAposentaBicicleta(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeAposentadoriaTest(){
        when(pabRepo.getPedidosAposentadoriaBicicleta()).thenReturn(new ArrayList<PedidoAposentaBicicleta>());

        bs.getPedidosDeAposentadoria();
    }
}
