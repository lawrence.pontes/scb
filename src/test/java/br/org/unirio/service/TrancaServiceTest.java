package br.org.unirio.service;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTranca;
import br.org.unirio.model.PedidoConsertoTranca;
import br.org.unirio.model.Tranca;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaTrancaRepository;
import br.org.unirio.repository.PedidoConsertoTrancaRepository;
import br.org.unirio.repository.TrancaRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class TrancaServiceTest {
    @Mock
    private TrancaRepository trancaRepo;
    @Mock
    private FuncionarioRepository funcionarioRepo;
    @Mock
    private PedidoConsertoTrancaRepository pctRepo;
    @Mock
    private PedidoAposentaTrancaRepository patRepo;
    @InjectMocks
    private TrancaService ts = new TrancaService();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    public void criaPedidoConsertoTrancaSucesso(){
//        doReturn(new Tranca()).when(trancaRepo).getTrancaById(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(pctRepo).getPedidoConsertoTranca(any(PedidoConsertoTranca.class));
//        doNothing().when(pctRepo).adicionaPedidoConsertoTranca(any(PedidoConsertoTranca.class));
//        try {
//            ts.criaPedidoConsertaTranca(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }

//    @Test
//    public void criaPedidoConsertoTrancaFalha(){
//        when(trancaRepo.getTrancaById(any(UUID.class))).thenReturn(new Tranca());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(pctRepo.getPedidoConsertoTranca(any(PedidoConsertoTranca.class))).thenReturn(new PedidoConsertoTranca(new Tranca(), new Funcionario()));
//        doNothing().when(pctRepo).adicionaPedidoConsertoTranca(any(PedidoConsertoTranca.class));
//        try {
//            ts.criaPedidoConsertaTranca(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeConsertoTest(){
        when(pctRepo.getPedidosTranca()).thenReturn(new ArrayList<PedidoConsertoTranca>());

        ts.getPedidosDeConserto();
    }

//    @Test
//    public void criaPedidoAposentaTrancaSucesso(){
//        doReturn(new Tranca()).when(trancaRepo).getTrancaById(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(patRepo).getPedidoAposentaTranca(any(PedidoAposentaTranca.class));
//        doNothing().when(patRepo).adicionaPedidoAposentaTranca(any(PedidoAposentaTranca.class));
//        try {
//            ts.criaPedidoAposentaTranca(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }
//
//    @Test
//    public void criaPedidoAposentaTrancaFalha(){
//        when(trancaRepo.getTrancaById(any(UUID.class))).thenReturn(new Tranca());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(patRepo.getPedidoAposentaTranca(any(PedidoAposentaTranca.class))).thenReturn(new PedidoAposentaTranca(new Tranca(), new Funcionario()));
//        doNothing().when(patRepo).adicionaPedidoAposentaTranca(any(PedidoAposentaTranca.class));
//        try {
//            ts.criaPedidoAposentaTranca(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeAposentadoriaTest(){
        when(patRepo.getPedidosAposentaTranca()).thenReturn(new ArrayList<PedidoAposentaTranca>());

        ts.getPedidosDeAposentadoria();
    }
}
