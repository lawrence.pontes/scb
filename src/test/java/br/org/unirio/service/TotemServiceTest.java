package br.org.unirio.service;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Totem;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTotem;
import br.org.unirio.model.PedidoConsertoTotem;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaTotemRepository;
import br.org.unirio.repository.PedidoConsertoTotemRepository;
import br.org.unirio.repository.TotemRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

public class TotemServiceTest {
    @Mock
    private TotemRepository totemRepo;
    @Mock
    private FuncionarioRepository funcionarioRepo;
    @Mock
    private PedidoConsertoTotemRepository pctRepo;
    @Mock
    private PedidoAposentaTotemRepository patRepo;
    @InjectMocks
    private TotemService ts = new TotemService();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    public void criaPedidoConsertoTotemSucesso(){
//        doReturn(new Totem()).when(totemRepo).getTotemById(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(pctRepo).getPedidoConsertoTotem(any(PedidoConsertoTotem.class));
//        doNothing().when(pctRepo).adicionaPedidoConsertoTotem(any(PedidoConsertoTotem.class));
//        try {
//            ts.criaPedidoConsertaTotem(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }

//    @Test
//    public void criaPedidoConsertoTotemFalha(){
//        when(totemRepo.getTotemById(any(UUID.class))).thenReturn(new Totem());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(pctRepo.getPedidoConsertoTotem(any(PedidoConsertoTotem.class))).thenReturn(new PedidoConsertoTotem(new Totem(), new Funcionario()));
//        doNothing().when(pctRepo).adicionaPedidoConsertoTotem(any(PedidoConsertoTotem.class));
//        try {
//            ts.criaPedidoConsertaTotem(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeConsertoTest(){
        when(pctRepo.getPedidosTotem()).thenReturn(new ArrayList<PedidoConsertoTotem>());

        ts.getPedidosDeConserto();
    }

//    @Test
//    public void criaPedidoAposentaTotemSucesso(){
//        doReturn(new Totem()).when(totemRepo).getTotemById(any(UUID.class));
//        doReturn(new Funcionario()).when(funcionarioRepo).getFuncionarioById(any(UUID.class));
//        doReturn(null).when(patRepo).getPedidoAposentaTotem(any(PedidoAposentaTotem.class));
//        doNothing().when(patRepo).adicionaPedidoAposentaTotem(any(PedidoAposentaTotem.class));
//        try {
//            ts.criaPedidoAposentaTotem(UUID.randomUUID(), UUID.randomUUID());
//        } catch (ObjetoJaExisteException e){
//            Assert.fail();
//        }
//    }

//    @Test
//    public void criaPedidoAposentaTotemFalha(){
//        when(totemRepo.getTotemById(any(UUID.class))).thenReturn(new Totem());
//        when(funcionarioRepo.getFuncionarioById(any(UUID.class))).thenReturn(new Funcionario());
//        when(patRepo.getPedidoAposentaTotem(any(PedidoAposentaTotem.class))).thenReturn(new PedidoAposentaTotem(new Totem(), new Funcionario()));
//        doNothing().when(patRepo).adicionaPedidoAposentaTotem(any(PedidoAposentaTotem.class));
//        try {
//            ts.criaPedidoAposentaTotem(UUID.randomUUID(), UUID.randomUUID());
//            Assert.fail();
//        } catch (ObjetoJaExisteException e){
//
//        }
//    }

    @Test
    public void getPedidosDeAposentadoriaTest(){
        when(patRepo.getPedidosAposentaTotem()).thenReturn(new ArrayList<PedidoAposentaTotem>());

        ts.getPedidosDeAposentadoria();
    }
}
