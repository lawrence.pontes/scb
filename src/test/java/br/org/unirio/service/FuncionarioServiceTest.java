package br.org.unirio.service;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.repository.FuncionarioRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class FuncionarioServiceTest {
    @Mock
    private FuncionarioRepository funcionarioRepo;

    @InjectMocks
    private FuncionarioService fs = new FuncionarioService();
    private List<Funcionario> mockFuncionarios;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockFuncionarios = new ArrayList<>();
        mockFuncionarios.add(new Funcionario(UUID.fromString("d290f1ee-6c54-4b01-90e6-d701748f0851"), "João Silva", 123456789, true, null));
        mockFuncionarios.add(new Funcionario(UUID.fromString("eff4cb6e-64cb-467e-9579-a398da2089c4"), "João Silva", 987654321, true, null));
        mockFuncionarios.add(new Funcionario(UUID.fromString("949eb0d1-aca2-4785-89d5-3f4f3d7fc26b"), "Lawrence Tavares", 123456789, true, null));
        mockFuncionarios.add(new Funcionario(UUID.fromString("0dc52820-2670-4c23-99f1-30545f5998bd"), "Ana Beatriz", 666, true, null));
        mockFuncionarios.add(new Funcionario(UUID.fromString("7c827bd8-a683-4ec6-a4f7-fd374bdaf070"), "Cassio Cidrini", 1000, true, null));
    }

    @Test
    public void getFuncionarioSoComIdExistente(){
        Funcionario f = mockFuncionarios.get(2);
        doReturn(f).when(funcionarioRepo).getFuncionarioById(mockFuncionarios.get(2).getId());

        try {
            List<Funcionario> funcionarios = fs.getFuncionario(mockFuncionarios.get(2).getId(), null, 0);
            Assert.assertEquals(1, funcionarios.size());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void getFuncionarioSoComIdInexistente(){
        doReturn(null).when(funcionarioRepo).getFuncionarioById(mockFuncionarios.get(2).getId());

        try {
            fs.getFuncionario(mockFuncionarios.get(2).getId(), null, 0);
            Assert.fail();
        } catch (FuncionarioNotFoundException e) {
        }
    }

    @Test
    public void getFuncionarioExistenteComTodosCampos(){
        Funcionario f = mockFuncionarios.get(2);
        doReturn(f).when(funcionarioRepo).getFuncionarioById(mockFuncionarios.get(2).getId());

        try {
            List<Funcionario> funcionarios = fs.getFuncionario(mockFuncionarios.get(2).getId(), mockFuncionarios.get(2).getNome(), mockFuncionarios.get(2).getMatricula());
            Assert.assertEquals(1, funcionarios.size());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void getFuncionarioExistenteComNomeEMatriculaExistentes(){
        ArrayList<Funcionario> funcionariosTest = new ArrayList<>();
        funcionariosTest.add(mockFuncionarios.get(2));
        doReturn(funcionariosTest).when(funcionarioRepo).getFuncionarioByNomeAndMatricula(mockFuncionarios.get(2).getNome(), mockFuncionarios.get(2).getMatricula());

        try {
            List<Funcionario> funcionarios = fs.getFuncionario(null, mockFuncionarios.get(2).getNome(), mockFuncionarios.get(2).getMatricula());
            Assert.assertEquals(1, funcionarios.size());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void getFuncionarioExistenteComCombinacaoNomeEMatriculaInexistente(){
        ArrayList<Funcionario> funcionariosTest = new ArrayList<>();
        doReturn(funcionariosTest).when(funcionarioRepo).getFuncionarioByNomeAndMatricula(mockFuncionarios.get(2).getNome(), mockFuncionarios.get(2).getMatricula());

        try {
            fs.getFuncionario(null, mockFuncionarios.get(3).getNome(), mockFuncionarios.get(2).getMatricula());
            Assert.fail();
        } catch (FuncionarioNotFoundException e) {
        }
    }

    @Test
    public void getFuncionarioExistenteApenasComNomeExistente(){
        ArrayList<Funcionario> funcionariosTest = new ArrayList<>();
        funcionariosTest.add(mockFuncionarios.get(0));
        funcionariosTest.add(mockFuncionarios.get(1));
        doReturn(funcionariosTest).when(funcionarioRepo).getFuncionarioByNomeAndMatricula(mockFuncionarios.get(1).getNome(), 0);

        try {
            List<Funcionario> funcionarios = fs.getFuncionario(null, mockFuncionarios.get(1).getNome(), 0);
            Assert.assertEquals(2, funcionarios.size());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void getFuncionarioExistenteApenasComMatriculaExistente(){
        ArrayList<Funcionario> funcionariosTest = new ArrayList<>();
        funcionariosTest.add(mockFuncionarios.get(0));
        funcionariosTest.add(mockFuncionarios.get(2));
        doReturn(funcionariosTest).when(funcionarioRepo).getFuncionarioByNomeAndMatricula(null, mockFuncionarios.get(0).getMatricula());

        try {
            List<Funcionario> funcionarios = fs.getFuncionario(null, null, mockFuncionarios.get(0).getMatricula());
            Assert.assertEquals(2, funcionarios.size());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void criaFuncionarioInexistente(){
        Funcionario f =  new Funcionario(mockFuncionarios.get(2).getId(), mockFuncionarios.get(0).getNome(), mockFuncionarios.get(3).getMatricula(), true, null);
        doReturn(null).when(funcionarioRepo).getFuncionario(f.getId(), f.getNome(), f.getMatricula());
        doNothing().when(funcionarioRepo).addFuncionario(f);

        try {
            fs.criaFuncionario(f);
        } catch (ObjetoJaExisteException e) {
            Assert.fail();
        }
    }

    @Test
    public void criaFuncionarioJaExistente(){
        Funcionario f =  new Funcionario(mockFuncionarios.get(2).getId(), mockFuncionarios.get(0).getNome(), mockFuncionarios.get(3).getMatricula(), true, null);
        doReturn(f).when(funcionarioRepo).getFuncionario(f.getId(), f.getNome(), f.getMatricula());
        doNothing().when(funcionarioRepo).addFuncionario(f);

        try {
            fs.criaFuncionario(f);
            Assert.fail();
        } catch (ObjetoJaExisteException e) {
        }
    }

    @Test
    public void deletaFuncionarioExistente(){
        Funcionario f = mockFuncionarios.get(2);
        doReturn(f).when(funcionarioRepo).getFuncionarioById(mockFuncionarios.get(2).getId());
        doNothing().when(funcionarioRepo).removeFuncionario(f);

        try {
            fs.deletaFuncionario(mockFuncionarios.get(2).getId());
        } catch (FuncionarioNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void deletaFuncionarioInexistente(){
        doReturn(null).when(funcionarioRepo).getFuncionarioById(mockFuncionarios.get(2).getId());
        doNothing().when(funcionarioRepo).removeFuncionario(null);

        try {
            fs.deletaFuncionario(mockFuncionarios.get(2).getId());
            Assert.fail();
        } catch (FuncionarioNotFoundException e) {
        }
    }
}
