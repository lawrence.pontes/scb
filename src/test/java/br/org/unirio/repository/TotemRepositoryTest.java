package br.org.unirio.repository;

import br.org.unirio.model.Totem;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.UUID;

public class TotemRepositoryTest {
    @InjectMocks
    TotemRepository totemRepo = TotemRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(totemRepo, TotemRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        totemRepo = null;
        Assert.assertNotNull(TotemRepository.getInstance());
    }

//    @Test
//    public void getTotemByIDExistente(){
//        Totem b = new Totem();
//        b.setId(UUID.randomUUID());
//        ArrayList<Totem> totems = new ArrayList<>();
//        totems.add(b);
//        totemRepo.setTotems(totems);
//
//        Assert.assertEquals(b, totemRepo.getTotemById(b.getId()));
//    }

//    @Test
//    public void getTotemByIDInexistente(){
//        Totem b = new Totem();
//        b.setId(UUID.randomUUID());
//        ArrayList<Totem> totems = new ArrayList<>();
//        totems.add(b);
//        totemRepo.setTotems(totems);
//        Totem totemRetornada = totemRepo.getTotemById(UUID.randomUUID());
//        Assert.assertNotNull(totemRetornada);
//        Assert.assertNotEquals(b, totemRetornada);
//    }
}
