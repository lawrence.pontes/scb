package br.org.unirio.repository;

import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoConsertoTranca;
import br.org.unirio.model.Tranca;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoConsertoTrancaRepositoryTest {
    @InjectMocks
    PedidoConsertoTrancaRepository pabRepo = PedidoConsertoTrancaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoConsertoTrancaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoConsertoTrancaRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosTranca().size();
        pabRepo.adicionaPedidoConsertoTranca(new PedidoConsertoTranca(new Tranca(), new Funcionario()));

        Assert.assertTrue(pabRepo.getPedidosTranca().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoConsertoTrancaExistente(){
        Funcionario funcionario = new Funcionario();
        Tranca bike = new Tranca();
        PedidoConsertoTranca pab = new PedidoConsertoTranca(bike, funcionario);
        ArrayList<PedidoConsertoTranca> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoConsertoTranca(pab));
    }

    @Test
    public void getPedidoConsertoTrancaInexistente(){
        Funcionario funcionario = new Funcionario();
        Tranca bike = new Tranca();
        PedidoConsertoTranca pab = new PedidoConsertoTranca(bike, funcionario);
        ArrayList<PedidoConsertoTranca> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoConsertoTranca(new PedidoConsertoTranca(new Tranca(), new Funcionario())));
    }
}
