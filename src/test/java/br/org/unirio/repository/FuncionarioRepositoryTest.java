package br.org.unirio.repository;

import br.org.unirio.model.Funcionario;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FuncionarioRepositoryTest {
    @InjectMocks
    FuncionarioRepository funcionarioRepo = FuncionarioRepository.getInstance();

    private ArrayList<Funcionario> funcionarios;

    @Before
    public void init(){
        this.funcionarios = new ArrayList<>();
        this.funcionarios.add(new Funcionario(UUID.fromString("d290f1ee-6c54-4b01-90e6-d701748f0851"), "João Silva", 123456789, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("eff4cb6e-64cb-467e-9579-a398da2089c4"), "Joana Silva", 987654321, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("949eb0d1-aca2-4785-89d5-3f4f3d7fc26b"), "Lawrence Tavares", 123123123, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("0dc52820-2670-4c23-99f1-30545f5998bd"), "Ana Beatriz", 666, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("7c827bd8-a683-4ec6-a4f7-fd374bdaf070"), "Cassio Cidrini", 1000, true, null));
    }

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(funcionarioRepo, FuncionarioRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        funcionarioRepo = null;
        Assert.assertNotNull(FuncionarioRepository.getInstance());
    }

    @Test
    public void getFuncionarioByIDExistente(){
        Funcionario b = new Funcionario();
        b.setId(UUID.randomUUID());
        ArrayList<Funcionario> funcionarios = new ArrayList<>();
        funcionarios.add(b);
        funcionarioRepo.setFuncionarios(funcionarios);

        Assert.assertEquals(b, funcionarioRepo.getFuncionarioById(b.getId()));
    }

    @Test
    public void getFuncionarioByIDInexistente(){
        Funcionario b = new Funcionario();
        b.setId(UUID.randomUUID());
        ArrayList<Funcionario> funcionarios = new ArrayList<>();
        funcionarios.add(b);
        funcionarioRepo.setFuncionarios(funcionarios);
        Funcionario funcionarioRetornado = funcionarioRepo.getFuncionarioById(UUID.randomUUID());
        Assert.assertNull(funcionarioRetornado);
        Assert.assertNotEquals(b, funcionarioRetornado);
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = funcionarioRepo.getFuncionarios().size();
        funcionarioRepo.addFuncionario(new Funcionario());

        Assert.assertTrue(funcionarioRepo.getFuncionarios().size() > tamanhoInicial);
    }

    @Test
    public void removeFuncionarioExistenteTest(){
        final int tamanhoInicial = funcionarioRepo.getFuncionarios().size();
        funcionarioRepo.removeFuncionario(funcionarioRepo.getFuncionarios().get(0));

        Assert.assertTrue(funcionarioRepo.getFuncionarios().size() < tamanhoInicial);
    }

    @Test
    public void removeFuncionarioInexistenteTest(){
        final int tamanhoInicial = funcionarioRepo.getFuncionarios().size();
        funcionarioRepo.removeFuncionario(new Funcionario());

        Assert.assertTrue(funcionarioRepo.getFuncionarios().size() == tamanhoInicial);
    }

    @Test
    public void getFuncionarioExistente(){
        funcionarioRepo.setFuncionarios(funcionarios);
        Assert.assertEquals(funcionarios.get(0), funcionarioRepo.getFuncionario(funcionarios.get(0).getId(), funcionarios.get(0).getNome(), funcionarios.get(0).getMatricula()));
    }

    @Test
    public void getFuncionarioInexistente(){
        funcionarioRepo.setFuncionarios(funcionarios);
        Assert.assertNull(funcionarioRepo.getFuncionario(UUID.randomUUID(), "bla", 0));
    }

    @Test
    public void getFuncionarioByNomeAndMatriculaExistentes(){
        funcionarioRepo.setFuncionarios(funcionarios);
        ArrayList<Funcionario> resultado = new ArrayList<>();
        resultado.add(funcionarios.get(0));
        Assert.assertEquals(resultado, funcionarioRepo.getFuncionarioByNomeAndMatricula(funcionarios.get(0).getNome(), funcionarios.get(0).getMatricula()));
    }

    @Test
    public void getFuncionarioByNomeAndMatriculaInexistentes(){
        funcionarioRepo.setFuncionarios(funcionarios);
        ArrayList<Funcionario> resultado = new ArrayList<>();
        resultado.add(funcionarios.get(0));
        Assert.assertTrue(funcionarioRepo.getFuncionarioByNomeAndMatricula("bla", 1).size() == 0);
    }
}
