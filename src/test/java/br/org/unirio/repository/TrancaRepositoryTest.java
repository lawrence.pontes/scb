package br.org.unirio.repository;

import br.org.unirio.model.Tranca;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.UUID;

public class TrancaRepositoryTest {
    @InjectMocks
    TrancaRepository trancaRepo = TrancaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(trancaRepo, TrancaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        trancaRepo = null;
        Assert.assertNotNull(TrancaRepository.getInstance());
    }

//    @Test
//    public void getTrancaByIDExistente(){
//        Tranca b = new Tranca();
//        b.setId(UUID.randomUUID());
//        ArrayList<Tranca> trancas = new ArrayList<>();
//        trancas.add(b);
//        trancaRepo.setTrancas(trancas);
//
//        Assert.assertEquals(b, trancaRepo.getTrancaById(b.getId()));
//    }
//
//    @Test
//    public void getTrancaByIDInexistente(){
//        Tranca b = new Tranca();
//        b.setId(UUID.randomUUID());
//        ArrayList<Tranca> trancas = new ArrayList<>();
//        trancas.add(b);
//        trancaRepo.setTrancas(trancas);
//        Tranca trancaRetornada = trancaRepo.getTrancaById(UUID.randomUUID());
//        Assert.assertNotNull(trancaRetornada);
//        Assert.assertNotEquals(b, trancaRetornada);
//    }
}
