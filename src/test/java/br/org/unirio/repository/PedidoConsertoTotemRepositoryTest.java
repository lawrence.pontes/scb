package br.org.unirio.repository;

import br.org.unirio.model.Totem;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoConsertoTotem;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoConsertoTotemRepositoryTest {
    @InjectMocks
    PedidoConsertoTotemRepository pabRepo = PedidoConsertoTotemRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoConsertoTotemRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoConsertoTotemRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosTotem().size();
        pabRepo.adicionaPedidoConsertoTotem(new PedidoConsertoTotem(new Totem(), new Funcionario()));

        Assert.assertTrue(pabRepo.getPedidosTotem().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoConsertoTotemExistente(){
        Funcionario funcionario = new Funcionario();
        Totem bike = new Totem();
        PedidoConsertoTotem pab = new PedidoConsertoTotem(bike, funcionario);
        ArrayList<PedidoConsertoTotem> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoConsertoTotem(pab));
    }

    @Test
    public void getPedidoConsertoTotemInexistente(){
        Funcionario funcionario = new Funcionario();
        Totem bike = new Totem();
        PedidoConsertoTotem pab = new PedidoConsertoTotem(bike, funcionario);
        ArrayList<PedidoConsertoTotem> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoConsertoTotem(new PedidoConsertoTotem(new Totem(), new Funcionario())));
    }
}
