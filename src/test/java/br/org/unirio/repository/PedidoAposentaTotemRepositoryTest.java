package br.org.unirio.repository;

import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTotem;
import br.org.unirio.model.Totem;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoAposentaTotemRepositoryTest {
    @InjectMocks
    PedidoAposentaTotemRepository pabRepo = PedidoAposentaTotemRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoAposentaTotemRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoAposentaTotemRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosAposentaTotem().size();
        pabRepo.adicionaPedidoAposentaTotem(new PedidoAposentaTotem(new Totem(), new Funcionario()));

        Assert.assertTrue(pabRepo.getPedidosAposentaTotem().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoAposentaTotemExistente(){
        Funcionario funcionario = new Funcionario();
        Totem bike = new Totem();
        PedidoAposentaTotem pab = new PedidoAposentaTotem(bike, funcionario);
        ArrayList<PedidoAposentaTotem> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoAposentaTotem(pab));
    }

    @Test
    public void getPedidoAposentaTotemInexistente(){
        Funcionario funcionario = new Funcionario();
        Totem bike = new Totem();
        PedidoAposentaTotem pab = new PedidoAposentaTotem(bike, funcionario);
        ArrayList<PedidoAposentaTotem> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoAposentaTotem(new PedidoAposentaTotem(new Totem(), new Funcionario())));
    }
}
