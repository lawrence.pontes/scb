package br.org.unirio.repository;

import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaBicicleta;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoAposentaBicicletaRepositoryTest {
    @InjectMocks
    PedidoAposentaBicicletaRepository pabRepo = PedidoAposentaBicicletaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoAposentaBicicletaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoAposentaBicicletaRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosAposentadoriaBicicleta().size();
        pabRepo.adicionaPedidoAposentaBicicleta(new PedidoAposentaBicicleta());

        Assert.assertTrue(pabRepo.getPedidosAposentadoriaBicicleta().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoAposentaBicicletaExistente(){
        Funcionario funcionario = new Funcionario();
        Bicicleta bike = new Bicicleta();
        PedidoAposentaBicicleta pab = new PedidoAposentaBicicleta(bike, funcionario);
        ArrayList<PedidoAposentaBicicleta> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoAposentaBicicleta(pab));
    }

    @Test
    public void getPedidoAposentaBicicletaInexistente(){
        Funcionario funcionario = new Funcionario();
        Bicicleta bike = new Bicicleta();
        PedidoAposentaBicicleta pab = new PedidoAposentaBicicleta(bike, funcionario);
        ArrayList<PedidoAposentaBicicleta> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoAposentaBicicleta(new PedidoAposentaBicicleta()));
    }
}
