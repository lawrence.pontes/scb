package br.org.unirio.repository;

import br.org.unirio.model.Bicicleta;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.UUID;

public class BicicletaRepositoryTest {
    @InjectMocks
    BicicletaRepository bikeRepo = BicicletaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(bikeRepo, BicicletaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        bikeRepo = null;
        Assert.assertNotNull(BicicletaRepository.getInstance());
    }

//    @Test
//    public void getBicicletaByIDExistente(){
//        Bicicleta b = new Bicicleta();
//        b.setId(UUID.randomUUID());
//        ArrayList<Bicicleta> bicicletas = new ArrayList<>();
//        bicicletas.add(b);
//        bikeRepo.setBicicletas(bicicletas);
//
//        Assert.assertEquals(b, bikeRepo.getBicicletaByID(b.getId()));
//    }
//
//    @Test
//    public void getBicicletaByIDInexistente(){
//        Bicicleta b = new Bicicleta();
//        b.setId(UUID.randomUUID());
//        ArrayList<Bicicleta> bicicletas = new ArrayList<>();
//        bicicletas.add(b);
//        bikeRepo.setBicicletas(bicicletas);
//        Bicicleta bicicletaRetornada = bikeRepo.getBicicletaByID(UUID.randomUUID());
//        Assert.assertNotNull(bicicletaRetornada);
//        Assert.assertNotEquals(b, bicicletaRetornada);
//    }
}
