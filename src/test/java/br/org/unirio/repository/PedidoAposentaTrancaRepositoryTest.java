package br.org.unirio.repository;

import br.org.unirio.model.Tranca;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaTranca;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoAposentaTrancaRepositoryTest {
    @InjectMocks
    PedidoAposentaTrancaRepository pabRepo = PedidoAposentaTrancaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoAposentaTrancaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoAposentaTrancaRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosAposentaTranca().size();
        pabRepo.adicionaPedidoAposentaTranca(new PedidoAposentaTranca(new Tranca(), new Funcionario()));

        Assert.assertTrue(pabRepo.getPedidosAposentaTranca().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoAposentaTrancaExistente(){
        Funcionario funcionario = new Funcionario();
        Tranca bike = new Tranca();
        PedidoAposentaTranca pab = new PedidoAposentaTranca(bike, funcionario);
        ArrayList<PedidoAposentaTranca> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoAposentaTranca(pab));
    }

    @Test
    public void getPedidoAposentaTrancaInexistente(){
        Funcionario funcionario = new Funcionario();
        Tranca bike = new Tranca();
        PedidoAposentaTranca pab = new PedidoAposentaTranca(bike, funcionario);
        ArrayList<PedidoAposentaTranca> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoAposentaTranca(new PedidoAposentaTranca(new Tranca(), new Funcionario())));
    }
}
