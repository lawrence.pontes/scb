package br.org.unirio.repository;

import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoConsertoBicicleta;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;

public class PedidoConsertaBicicletaRepositoryTest {
    @InjectMocks
    PedidoConsertoBicicletaRepository pabRepo = PedidoConsertoBicicletaRepository.getInstance();

    @Test
    public void getInstanceInstanciadoTest(){
        Assert.assertEquals(pabRepo, PedidoConsertoBicicletaRepository.getInstance());
    }

    @Test
    public void getInstanceNaoInstanciadoTest(){
        pabRepo = null;
        Assert.assertNotNull(PedidoConsertoBicicletaRepository.getInstance());
    }

    @Test
    public void adicionaFuncionarioTest(){
        final int tamanhoInicial = pabRepo.getPedidosBicicleta().size();
        pabRepo.adicionaPedidoConsertoBicicleta(new PedidoConsertoBicicleta(new Bicicleta(), new Funcionario()));

        Assert.assertTrue(pabRepo.getPedidosBicicleta().size() > tamanhoInicial);
    }

    @Test
    public void getPedidoConsertoBicicletaExistente(){
        Funcionario funcionario = new Funcionario();
        Bicicleta bike = new Bicicleta();
        PedidoConsertoBicicleta pab = new PedidoConsertoBicicleta(bike, funcionario);
        ArrayList<PedidoConsertoBicicleta> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertEquals(pab, pabRepo.getPedidoConsertoBicicleta(pab));
    }

    @Test
    public void getPedidoConsertoBicicletaInexistente(){
        Funcionario funcionario = new Funcionario();
        Bicicleta bike = new Bicicleta();
        PedidoConsertoBicicleta pab = new PedidoConsertoBicicleta(bike, funcionario);
        ArrayList<PedidoConsertoBicicleta> pedidos = new ArrayList<>();
        pedidos.add(pab);
        pabRepo.setPedidos(pedidos);
        Assert.assertNull(pabRepo.getPedidoConsertoBicicleta(new PedidoConsertoBicicleta(new Bicicleta(), new Funcionario())));
    }
}
