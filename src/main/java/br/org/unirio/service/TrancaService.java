package br.org.unirio.service;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.*;
import br.org.unirio.repository.*;
import br.org.unirio.util.Status;

import java.util.ArrayList;
import java.util.UUID;

public class TrancaService {

    private FuncionarioRepository funcRepo = FuncionarioRepository.getInstance();
    private TrancaRepository trancaRepo = TrancaRepository.getInstance();
    private PedidoConsertoTrancaRepository pctRepo = PedidoConsertoTrancaRepository.getInstance();
    private PedidoAposentaTrancaRepository patRepo = PedidoAposentaTrancaRepository.getInstance();

    public void criaPedidoConsertaTranca(int idTranca, UUID idFuncionario) throws ObjetoJaExisteException, FuncionarioNotFoundException {
        Tranca tranca = trancaRepo.getTrancaById(idTranca);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        if(funcionario == null || tranca == null)
            throw new FuncionarioNotFoundException();

        if(tranca.getStatus().equalsIgnoreCase("Em Reparo"))
            throw new ObjetoJaExisteException();
        trancaRepo.alteraStatusTranca(idTranca, "Em Reparo");
    }

    public ArrayList<PedidoConsertoTranca> getPedidosDeConserto() {
        return pctRepo.getPedidosTranca();
    }

    public void criaPedidoAposentaTranca(int idTranca, UUID idFuncionario) throws ObjetoJaExisteException, FuncionarioNotFoundException {
        Tranca tranca = trancaRepo.getTrancaById(idTranca);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        if(funcionario == null || tranca == null)
            throw new FuncionarioNotFoundException();

        if(tranca.getStatus().equalsIgnoreCase("Aposentada"))
            throw new ObjetoJaExisteException();
        trancaRepo.alteraStatusTranca(idTranca, "Aposentada");
    }

    public ArrayList<PedidoAposentaTranca> getPedidosDeAposentadoria() {
        return patRepo.getPedidosAposentaTranca();
    }
}
