package br.org.unirio.service;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.*;
import br.org.unirio.repository.*;

import java.util.ArrayList;
import java.util.UUID;

public class TotemService {
    private TotemRepository totemRepo = TotemRepository.getInstance();
    private FuncionarioRepository funcRepo = FuncionarioRepository.getInstance();
    private PedidoConsertoTotemRepository pctRepo = PedidoConsertoTotemRepository.getInstance();
    private PedidoAposentaTotemRepository patRepo = PedidoAposentaTotemRepository.getInstance();

    public void criaPedidoConsertaTotem(int idTotem, UUID idFuncionario) throws ObjetoJaExisteException {
        Totem totem = totemRepo.getTotemById(idTotem);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        PedidoConsertoTotem pedido = new PedidoConsertoTotem(totem, funcionario);
        if(pctRepo.getPedidoConsertoTotem(pedido) != null)
            throw new ObjetoJaExisteException();
        pctRepo.adicionaPedidoConsertoTotem(pedido);
    }

    public ArrayList<PedidoConsertoTotem> getPedidosDeConserto() {
        return pctRepo.getPedidosTotem();
    }

    public void criaPedidoAposentaTotem(int idTotem, UUID idFuncionario) throws ObjetoJaExisteException {
        Totem totem = totemRepo.getTotemById(idTotem);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        PedidoAposentaTotem pedido = new PedidoAposentaTotem(totem, funcionario);
        if(patRepo.getPedidoAposentaTotem(pedido) != null)
            throw new ObjetoJaExisteException();
        patRepo.adicionaPedidoAposentaTotem(pedido);
    }

    public ArrayList<PedidoAposentaTotem> getPedidosDeAposentadoria() {
        return patRepo.getPedidosAposentaTotem();
    }
}
