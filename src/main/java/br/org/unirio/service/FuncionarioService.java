package br.org.unirio.service;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.repository.FuncionarioRepository;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FuncionarioService {

    private FuncionarioRepository funcRepo = FuncionarioRepository.getInstance();

    public List<Funcionario> getFuncionario(UUID id, String nome, long matricula) throws FuncionarioNotFoundException {
        List<Funcionario> funcionarios = new ArrayList<>();
        if(id != null) {
            Funcionario funcionario = funcRepo.getFuncionarioById(id);
            if(funcionario != null && (StringUtils.isEmpty(nome) || nome.equalsIgnoreCase(funcionario.getNome()))
                    && (matricula == 0 || matricula == funcionario.getMatricula()))
                funcionarios.add(funcionario);
        } else {
            funcionarios.addAll(funcRepo.getFuncionarioByNomeAndMatricula(nome, matricula));
        }

        if(funcionarios.size() == 0)
            throw new FuncionarioNotFoundException();

        return funcionarios;
    }

    public void criaFuncionario(Funcionario funcionarioACriar) throws ObjetoJaExisteException {
        Funcionario funcionario = funcRepo.getFuncionario(funcionarioACriar.getId(),
                funcionarioACriar.getNome(), funcionarioACriar.getMatricula());

        if(funcionario != null)
            throw new ObjetoJaExisteException();

        funcRepo.addFuncionario(funcionarioACriar);
    }

    public void deletaFuncionario(UUID id) throws FuncionarioNotFoundException {
        Funcionario funcionario = funcRepo.getFuncionarioById(id);

        if(funcionario == null)
            throw new FuncionarioNotFoundException();

        funcRepo.removeFuncionario(funcionario);
    }
}
