package br.org.unirio.service;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Funcionario;
import br.org.unirio.model.PedidoAposentaBicicleta;
import br.org.unirio.model.PedidoConsertoBicicleta;
import br.org.unirio.repository.BicicletaRepository;
import br.org.unirio.repository.FuncionarioRepository;
import br.org.unirio.repository.PedidoAposentaBicicletaRepository;
import br.org.unirio.repository.PedidoConsertoBicicletaRepository;
import br.org.unirio.util.Status;

import java.util.ArrayList;
import java.util.UUID;

public class BicicletaService {

    private BicicletaRepository bikeRepo = BicicletaRepository.getInstance();
    private FuncionarioRepository funcRepo = FuncionarioRepository.getInstance();
    private PedidoConsertoBicicletaRepository pcbRepo = PedidoConsertoBicicletaRepository.getInstance();
    private PedidoAposentaBicicletaRepository pabRepo = PedidoAposentaBicicletaRepository.getInstance();

    public void criaPedidoConsertoBicicleta(int idBicicleta, UUID idFuncionario) throws ObjetoJaExisteException, FuncionarioNotFoundException {
        Bicicleta bicicleta = bikeRepo.existeBicicletaByID(idBicicleta);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        if(funcionario == null || bicicleta == null)
            throw new FuncionarioNotFoundException();

        if(bicicleta.getStatus().equalsIgnoreCase(Status.EM_REPARO.getChave()))
            throw new ObjetoJaExisteException();
        bikeRepo.alteraStatusBike(idBicicleta, Status.EM_REPARO.getChave());
    }

    public ArrayList<PedidoConsertoBicicleta> getPedidosDeConserto() {
        return pcbRepo.getPedidosBicicleta();
    }

    public void criaPedidoAposentaBicicleta(int  idBicicleta, UUID idFuncionario) throws ObjetoJaExisteException, FuncionarioNotFoundException {
        Bicicleta bicicleta = bikeRepo.existeBicicletaByID(idBicicleta);
        Funcionario funcionario = funcRepo.getFuncionarioById(idFuncionario);

        if(funcionario == null || bicicleta == null)
            throw new FuncionarioNotFoundException();

        if(bicicleta.getStatus().equalsIgnoreCase(Status.APOSENTADA.getChave()))
            throw new ObjetoJaExisteException();
        bikeRepo.alteraStatusBike(idBicicleta, Status.APOSENTADA.getChave());
    }

    public ArrayList<PedidoAposentaBicicleta> getPedidosDeAposentadoria() {
        return pabRepo.getPedidosAposentadoriaBicicleta();
    }
}
