package br.org.unirio.controller;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.service.BicicletaService;
import io.javalin.http.Context;

import java.util.UUID;

public class BicicletaController {
    private BicicletaController() {}

    public static void consertaBicicleta(Context ctx) {
        BicicletaService bicicletaService = new BicicletaService();
        int idBicicleta;
        UUID idFuncionario;

        try{
            idBicicleta = Integer.parseInt(ctx.queryParam("Bicicleta"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            bicicletaService.criaPedidoConsertoBicicleta(idBicicleta, idFuncionario);
            ctx.status(201);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getConsertosBicicleta(Context ctx) {
        BicicletaService bicicletaService = new BicicletaService();
        ctx.json(bicicletaService.getPedidosDeConserto());
    }

    public static void aposentaBicicleta(Context ctx) {
        BicicletaService bicicletaService = new BicicletaService();
        int idBicicleta;
        UUID idFuncionario;

        try{
            idBicicleta = Integer.parseInt(ctx.queryParam("Bicicleta"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            bicicletaService.criaPedidoAposentaBicicleta(idBicicleta, idFuncionario);
            ctx.status(201);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getAposentadas(Context ctx) {
        BicicletaService bicicletaService = new BicicletaService();
        ctx.json(bicicletaService.getPedidosDeAposentadoria());
    }
}
