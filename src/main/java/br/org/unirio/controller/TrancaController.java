package br.org.unirio.controller;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.service.BicicletaService;
import br.org.unirio.service.TrancaService;
import io.javalin.http.Context;

import java.util.UUID;

public class TrancaController {
    private TrancaController(){}

    public static void consertaTranca(Context ctx) {
        TrancaService trancaService = new TrancaService();
        int idTranca;
        UUID idFuncionario;

        try{
            idTranca = Integer.parseInt(ctx.queryParam("Tranca"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            trancaService.criaPedidoConsertaTranca(idTranca, idFuncionario);
            ctx.status(200);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getConsertosTranca(Context ctx) {
        TrancaService trancaService = new TrancaService();
        ctx.json(trancaService.getPedidosDeConserto());
    }

    public static void aposentaTranca(Context ctx) {
        TrancaService trancaService = new TrancaService();
        int idTranca;
        UUID idFuncionario;

        try{
            idTranca = Integer.parseInt(ctx.queryParam("Tranca"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            trancaService.criaPedidoAposentaTranca(idTranca, idFuncionario);
            ctx.status(200);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getAposentadas(Context ctx) {
        TrancaService trancaService = new TrancaService();
        ctx.json(trancaService.getPedidosDeAposentadoria());
    }
}
