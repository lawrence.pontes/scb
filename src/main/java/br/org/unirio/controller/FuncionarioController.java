package br.org.unirio.controller;

import br.org.unirio.exception.FuncionarioNotFoundException;
import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.model.Funcionario;
import br.org.unirio.service.FuncionarioService;
import br.org.unirio.util.ConversorJson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.javalin.http.Context;

import java.util.Objects;
import java.util.UUID;

public class FuncionarioController {
    private FuncionarioController() {}

    public static void getFuncionario(Context ctx) {
        FuncionarioService fs = new FuncionarioService();

        UUID id;
        String nome;
        int matricula;
        try {
            id = UUID.fromString(ctx.queryParam("id"));
        } catch (Exception e) {
            id = null;
        }

        try{
            nome = ctx.queryParam("nome");
        }catch (Exception e) {
            nome = null;
        }

        try{
            matricula = Integer.valueOf(ctx.queryParam("matricula"));
        }catch (Exception e) {
            matricula = 0;
        }

        try{
            ctx.json(fs.getFuncionario(id, nome, matricula));
            ctx.status(200);
        } catch (FuncionarioNotFoundException fnfe){
            ctx.status(404);
            return;
        }
    }

    public static void criaFuncionario(Context ctx) {
        FuncionarioService fs = new FuncionarioService();

        try {
            JsonObject jsonFuncionario = new JsonParser().parse(ctx.body()).getAsJsonObject();
            Funcionario funcionarioACriar = ConversorJson.converteJsonParaFuncionario(jsonFuncionario);
            fs.criaFuncionario(funcionarioACriar);
            ctx.status(201);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void deletaFuncionario(Context ctx) {
        FuncionarioService fs = new FuncionarioService();
        UUID id = UUID.fromString(Objects.requireNonNull(ctx.queryParam("id")));

        try{
            fs.deletaFuncionario(id);
            ctx.status(200);
        } catch (FuncionarioNotFoundException fnfe){
            ctx.status(404);
            return;
        }
    }
}
