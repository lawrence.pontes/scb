package br.org.unirio.controller;

import br.org.unirio.exception.ObjetoJaExisteException;
import br.org.unirio.service.TotemService;
import br.org.unirio.service.TotemService;
import io.javalin.http.Context;

import java.util.UUID;

public class TotemController {
    private TotemController(){}

    public static void consertaTotem(Context ctx) {
        TotemService totemService = new TotemService();
        int idTotem;
        UUID idFuncionario;

        try{
            idTotem = Integer.parseInt(ctx.queryParam("Totem"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            totemService.criaPedidoConsertaTotem(idTotem, idFuncionario);
            ctx.status(200);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getConsertosTotem(Context ctx) {
        TotemService totemService = new TotemService();
        ctx.json(totemService.getPedidosDeConserto());
    }

    public static void aposentaTotem(Context ctx) {
        TotemService totemService = new TotemService();
        int idTotem;
        UUID idFuncionario;

        try{
            idTotem = Integer.parseInt(ctx.queryParam("Totem"));
            idFuncionario = UUID.fromString(ctx.queryParam("Funcionario"));
            totemService.criaPedidoAposentaTotem(idTotem, idFuncionario);
            ctx.status(200);
        } catch (ObjetoJaExisteException e){
            ctx.status(409);
        } catch (Exception e){
            ctx.status(400);
        }
    }

    public static void getAposentados(Context ctx) {
        TotemService totemService = new TotemService();
        ctx.json(totemService.getPedidosDeAposentadoria());
    }
}
