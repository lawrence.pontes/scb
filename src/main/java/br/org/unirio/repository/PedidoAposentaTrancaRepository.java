package br.org.unirio.repository;

import br.org.unirio.model.PedidoAposentaTranca;

import java.util.ArrayList;

public class PedidoAposentaTrancaRepository {
    private static PedidoAposentaTrancaRepository instance;
    private ArrayList<PedidoAposentaTranca> pedidos;

    private PedidoAposentaTrancaRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoAposentaTrancaRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoAposentaTrancaRepository();
        return instance;
    }

    public void adicionaPedidoAposentaTranca(PedidoAposentaTranca pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoAposentaTranca> getPedidosAposentaTranca(){
        return pedidos;
    }

    public PedidoAposentaTranca getPedidoAposentaTranca(PedidoAposentaTranca pedidoConsertoTranca){
        for (PedidoAposentaTranca pedido: pedidos) {
            if(pedido.getTrancaASerAposentada().equals(pedidoConsertoTranca.getTrancaASerAposentada()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoConsertoTranca.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoAposentaTranca> pedidos) {
        this.pedidos = pedidos;
    }
}
