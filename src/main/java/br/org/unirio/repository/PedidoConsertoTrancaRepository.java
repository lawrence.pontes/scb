package br.org.unirio.repository;

import br.org.unirio.model.PedidoConsertoBicicleta;
import br.org.unirio.model.PedidoConsertoTranca;

import java.util.ArrayList;

public class PedidoConsertoTrancaRepository {
    private static PedidoConsertoTrancaRepository instance;
    private ArrayList<PedidoConsertoTranca> pedidos;

    private PedidoConsertoTrancaRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoConsertoTrancaRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoConsertoTrancaRepository();
        return instance;
    }

    public void adicionaPedidoConsertoTranca(PedidoConsertoTranca pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoConsertoTranca> getPedidosTranca(){
        return pedidos;
    }

    public PedidoConsertoTranca getPedidoConsertoTranca(PedidoConsertoTranca pedidoConsertoTranca){
        for (PedidoConsertoTranca pedido: pedidos) {
            if(pedido.getTrancaDefeituosa().equals(pedidoConsertoTranca.getTrancaDefeituosa()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoConsertoTranca.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoConsertoTranca> pedidos) {
        this.pedidos = pedidos;
    }
}
