package br.org.unirio.repository;

import br.org.unirio.model.PedidoConsertoBicicleta;

import java.util.ArrayList;

public class PedidoConsertoBicicletaRepository {
    private static PedidoConsertoBicicletaRepository instance;
    private ArrayList<PedidoConsertoBicicleta> pedidos;

    private PedidoConsertoBicicletaRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoConsertoBicicletaRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoConsertoBicicletaRepository();
        return instance;
    }

    public void adicionaPedidoConsertoBicicleta(PedidoConsertoBicicleta pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoConsertoBicicleta> getPedidosBicicleta(){
        return pedidos;
    }

    public PedidoConsertoBicicleta getPedidoConsertoBicicleta(PedidoConsertoBicicleta pedidoConsertoBicicleta){
        for (PedidoConsertoBicicleta pedido: pedidos) {
            if(pedido.getBicicletaDefeituosa().equals(pedidoConsertoBicicleta.getBicicletaDefeituosa()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoConsertoBicicleta.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoConsertoBicicleta> pedidos) {
        this.pedidos = pedidos;
    }
}
