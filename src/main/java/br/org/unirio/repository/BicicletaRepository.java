package br.org.unirio.repository;

import br.org.unirio.model.Bicicleta;
import br.org.unirio.util.ConversorJson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class BicicletaRepository {
    private static final String BICICLETA_URI = "https://bicicletario.herokuapp.com";

    private static Client client = ClientBuilder.newClient();
    private static BicicletaRepository instance;

    public static BicicletaRepository getInstance(){
        if(instance == null)
            instance = new BicicletaRepository();
        return instance;
    }

    public Bicicleta existeBicicletaByID(int id){
        String response = client.target(BICICLETA_URI).path("bicicleta").queryParam("id", id).request(MediaType.APPLICATION_JSON).get(String.class);

        JsonObject jsonResponse = new JsonParser().parse(response).getAsJsonObject();

        if(jsonResponse.get("id") == null || jsonResponse.get("id").getAsInt() != id)
            return null;

        Bicicleta bicicleta = ConversorJson.converteJsonParaBicicleta(jsonResponse);

        return bicicleta;
    }

    public void alteraStatusBike(int idBicicleta, String status) {

        client.target(BICICLETA_URI).path("statusBicicleta").queryParam("id", idBicicleta).queryParam("status", status).request(MediaType.APPLICATION_JSON).post(null);
    }
}
