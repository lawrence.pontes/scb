package br.org.unirio.repository;

import br.org.unirio.model.PedidoAposentaTotem;

import java.util.ArrayList;

public class PedidoAposentaTotemRepository {
    private static PedidoAposentaTotemRepository instance;
    private ArrayList<PedidoAposentaTotem> pedidos;

    private PedidoAposentaTotemRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoAposentaTotemRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoAposentaTotemRepository();
        return instance;
    }

    public void adicionaPedidoAposentaTotem(PedidoAposentaTotem pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoAposentaTotem> getPedidosAposentaTotem(){
        return pedidos;
    }

    public PedidoAposentaTotem getPedidoAposentaTotem(PedidoAposentaTotem pedidoConsertoTotem){
        for (PedidoAposentaTotem pedido: pedidos) {
            if(pedido.getTotemASerAposentado().equals(pedidoConsertoTotem.getTotemASerAposentado()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoConsertoTotem.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoAposentaTotem> pedidos) {
        this.pedidos = pedidos;
    }
}
