package br.org.unirio.repository;

import br.org.unirio.model.Funcionario;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class FuncionarioRepository {
    private List<Funcionario> funcionarios;
    private static FuncionarioRepository instance;

    public static FuncionarioRepository getInstance(){
        if(instance == null)
            instance = new FuncionarioRepository();
        return instance;
    }

    private FuncionarioRepository(){
        this.funcionarios = new ArrayList<>();
        this.funcionarios.add(new Funcionario(UUID.fromString("d290f1ee-6c54-4b01-90e6-d701748f0851"), "João Silva", 123456789, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("eff4cb6e-64cb-467e-9579-a398da2089c4"), "Joana Silva", 987654321, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("949eb0d1-aca2-4785-89d5-3f4f3d7fc26b"), "Lawrence Tavares", 123123123, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("0dc52820-2670-4c23-99f1-30545f5998bd"), "Ana Beatriz", 666, true, null));
        this.funcionarios.add(new Funcionario(UUID.fromString("7c827bd8-a683-4ec6-a4f7-fd374bdaf070"), "Cassio Cidrini", 1000, true, null));
    }

    public void addFuncionario(Funcionario funcionario){
        funcionarios.add(funcionario);
    }

    public Funcionario getFuncionario(UUID id, String nome, long matricula){
        for (Funcionario funcionario: funcionarios) {
            if(funcionario.getId().equals(id) && funcionario.getNome().equalsIgnoreCase(nome) && funcionario.getMatricula() == matricula)
                return funcionario;
        }

        return null;
    }

    public Funcionario getFuncionarioById(UUID id){
        for (Funcionario funcionario: funcionarios) {
            if(funcionario.getId().equals(id))
                return funcionario;
        }

        return null;
    }

    public void removeFuncionario(Funcionario funcionario){
        funcionarios.remove(funcionario);
    }

    public List<Funcionario> getFuncionarioByNomeAndMatricula(String nome, long matricula) {
        List<Funcionario> result = new ArrayList<>();
        for (Funcionario funcionario: funcionarios) {
            if((StringUtils.isEmpty(nome) || funcionario.getNome().equalsIgnoreCase(nome))
                    &&
                    (matricula == 0 || funcionario.getMatricula() == matricula))
                result.add(funcionario);
        }

        return result;
    }

    public void setFuncionarios(ArrayList<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public List<Funcionario> getFuncionarios() {
        return funcionarios;
    }
}
