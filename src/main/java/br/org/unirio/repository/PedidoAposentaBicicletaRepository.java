package br.org.unirio.repository;

import br.org.unirio.model.PedidoAposentaBicicleta;
import br.org.unirio.model.PedidoConsertoBicicleta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PedidoAposentaBicicletaRepository {
    private static PedidoAposentaBicicletaRepository instance;
    private ArrayList<PedidoAposentaBicicleta> pedidos;

    private PedidoAposentaBicicletaRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoAposentaBicicletaRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoAposentaBicicletaRepository();
        return instance;
    }

    public void adicionaPedidoAposentaBicicleta(PedidoAposentaBicicleta pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoAposentaBicicleta> getPedidosAposentadoriaBicicleta(){
        return pedidos;
    }

    public PedidoAposentaBicicleta getPedidoAposentaBicicleta(PedidoAposentaBicicleta pedidoAposentaBicicleta){
        for (PedidoAposentaBicicleta pedido: pedidos) {
            if(pedido.getBicicletaASerAposentada().equals(pedidoAposentaBicicleta.getBicicletaASerAposentada()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoAposentaBicicleta.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoAposentaBicicleta> pedidos) {
        this.pedidos = pedidos;
    }
}
