package br.org.unirio.repository;

import br.org.unirio.model.PedidoConsertoTotem;

import java.util.ArrayList;

public class PedidoConsertoTotemRepository {
    private static PedidoConsertoTotemRepository instance;
    private ArrayList<PedidoConsertoTotem> pedidos;

    private PedidoConsertoTotemRepository(){
        pedidos = new ArrayList<>();
    }

    public static PedidoConsertoTotemRepository getInstance(){
        if(instance != null)
            return instance;
        instance = new PedidoConsertoTotemRepository();
        return instance;
    }

    public void adicionaPedidoConsertoTotem(PedidoConsertoTotem pedido) {
        pedidos.add(pedido);
    }

    public ArrayList<PedidoConsertoTotem> getPedidosTotem(){
        return pedidos;
    }

    public PedidoConsertoTotem getPedidoConsertoTotem(PedidoConsertoTotem pedidoConsertoTotem){
        for (PedidoConsertoTotem pedido: pedidos) {
            if(pedido.getTotemDefeituosa().equals(pedidoConsertoTotem.getTotemDefeituosa()) &&
                    pedido.getFuncionarioRequisitante().equals(pedidoConsertoTotem.getFuncionarioRequisitante()))
                return pedido;
        }
        return null;
    }

    public void setPedidos(ArrayList<PedidoConsertoTotem> pedidos) {
        this.pedidos = pedidos;
    }
}
