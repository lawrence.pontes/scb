package br.org.unirio.repository;

import br.org.unirio.model.Totem;
import br.org.unirio.util.ConversorJson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class TotemRepository {
    private static final String TOTEM_URI = "https://javalin-heroku-equipamento.herokuapp.com";

    private static Client client = ClientBuilder.newClient();
    private static TotemRepository instance;

    public static TotemRepository getInstance(){
        if(instance == null)
            instance = new TotemRepository();
        return instance;
    }

    public Totem getTotemById(int id){
        String response = client.target(TOTEM_URI).path("totem/buscaPorId").queryParam("totemId", id).request(MediaType.APPLICATION_JSON).get(String.class);

        JsonObject jsonResponse = new JsonParser().parse(response).getAsJsonObject();

        if(jsonResponse.get("id") == null || jsonResponse.get("id").getAsInt() != id)
            return null;

        Totem totem = ConversorJson.converteJsonParaTotem(jsonResponse);
        return totem;
    }

}
