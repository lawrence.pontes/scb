package br.org.unirio.repository;

import br.org.unirio.model.Tranca;
import br.org.unirio.util.ConversorJson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class TrancaRepository {
    private static final String TRANCA_URI = "https://javalin-heroku-equipamento.herokuapp.com";

    private static Client client = ClientBuilder.newClient();
    private static TrancaRepository instance;

    public static TrancaRepository getInstance(){
        if(instance == null)
            instance = new TrancaRepository();
        return instance;
    }

    public Tranca getTrancaById(int id){
        String response = client.target(TRANCA_URI).path("tranca/buscaPorId").queryParam("trancaId", id).request(MediaType.APPLICATION_JSON).get(String.class);

        JsonObject jsonResponse = new JsonParser().parse(response).getAsJsonObject();

        if(jsonResponse.get("id") == null || jsonResponse.get("id").getAsInt() != id)
            return null;

        Tranca tranca = ConversorJson.converteJsonParaTranca(jsonResponse);

        return tranca;
    }

    public void alteraStatusTranca(int idTranca, String status) {
        client.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
        client.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
        client.target(TRANCA_URI).path("statusTranca").queryParam("trancaId", idTranca)
                .queryParam("statusTranca", status).request(MediaType.APPLICATION_JSON).method("PATCH", null, String.class);
    }
}
