package br.org.unirio.runner;

import br.org.unirio.controller.BicicletaController;
import br.org.unirio.controller.FuncionarioController;
import br.org.unirio.controller.TotemController;
import br.org.unirio.controller.TrancaController;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

public class ApplicationMain {
    public static void main(String[] args) {
        Javalin javalin = Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
        }).start(getHerokuAssignedPort());
        javalin.routes(() -> {
            path("funcionario", () -> {
                get(FuncionarioController::getFuncionario);
                post(FuncionarioController::criaFuncionario);
                delete(FuncionarioController::deletaFuncionario);
            });
            path("consertaBicicleta", () -> {
                post(BicicletaController::consertaBicicleta);
                get(BicicletaController::getConsertosBicicleta);
            });
            path("aposentaBicicleta", () -> {
                post(BicicletaController::aposentaBicicleta);
                get(BicicletaController::getAposentadas);
            });
            path("consertaTranca", () -> {
                post(TrancaController::consertaTranca);
                get(TrancaController::getConsertosTranca);
            });
            path("aposentaTranca", () -> {
                post(TrancaController::aposentaTranca);
                get(TrancaController::getAposentadas);
            });
            path("consertaTotem", () -> {
                post(TotemController::consertaTotem);
                get(TotemController::getConsertosTotem);
            });
            path("aposentaTotem", () -> {
                post(TotemController::aposentaTotem);
                get(TotemController::getAposentados);
            });
        });
    }

    private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").title("User API").description("Demo API with 5 operations");
        OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("io.javalin.example.java")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")); // endpoint for redoc
        return new OpenApiPlugin(options);
    }

    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
            return Integer.parseInt(herokuPort);
        }
        return 7000;
    }
}
