package br.org.unirio.util;

import br.org.unirio.model.*;
import com.google.gson.JsonObject;

import java.util.UUID;

public class ConversorJson {
    private ConversorJson(){}

    public static Funcionario converteJsonParaFuncionario(JsonObject jsonFuncionario){
        String id = jsonFuncionario.get("id").getAsString();
        String nome = jsonFuncionario.get("nome").getAsString();
        long matricula = jsonFuncionario.get("matricula").getAsInt();
        boolean indicadorBr = jsonFuncionario.get("indicadorBr").getAsBoolean();
        JsonObject jsonDocumento =  jsonFuncionario.get("documento").getAsJsonObject();
        String tipo = jsonDocumento.get("tipo").getAsString();
        long numero = jsonDocumento.get("numero").getAsInt();

        Documento documento = new Documento();
        documento.setNumero(numero);
        documento.setTipo(TipoDocumento.from(tipo));
        return new Funcionario(UUID.fromString(id), nome, matricula, indicadorBr, documento);
    }

    public static Bicicleta converteJsonParaBicicleta(JsonObject jsonBicicleta) {
        int id = jsonBicicleta.get("id").getAsInt();
        String status = jsonBicicleta.get("status").getAsString();
        int codigo = jsonBicicleta.get("code").getAsInt();

        Bicicleta bike = new Bicicleta();
        bike.setId(id);
        bike.setStatus(status);
        bike.setCodigo(codigo);

        return bike;
    }

    public static Totem converteJsonParaTotem(JsonObject jsonTotem) {
        int id = jsonTotem.get("id").getAsInt();
        int numero = jsonTotem.get("numero").getAsInt();

        Totem totem = new Totem();
        totem.setId(id);
        totem.setNumero(numero);

        return totem;
    }

    public static Tranca converteJsonParaTranca(JsonObject jsonTranca) {
        int id = jsonTranca.get("id").getAsInt();
        String status = jsonTranca.get("status").getAsString();
        int numero = jsonTranca.get("numero").getAsInt();

        Tranca tranca = new Tranca();
        tranca.setId(id);
        tranca.setStatus(status);
        tranca.setNumero(numero);

        return tranca;
    }
}
