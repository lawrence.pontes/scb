package br.org.unirio.model;

public class PedidoAposentaTotem {
    private Funcionario funcionarioRequisisante;
    private Totem totemASerAposentado;

    public PedidoAposentaTotem(Totem totem, Funcionario funcionario) {
        this.totemASerAposentado = totem;
        this.funcionarioRequisisante = funcionario;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisisante;
    }

    public void setFuncionarioRequisisante(Funcionario funcionarioRequisisante) {
        this.funcionarioRequisisante = funcionarioRequisisante;
    }

    public Totem getTotemASerAposentado() {
        return totemASerAposentado;
    }

    public void setTotemASerAposentado(Totem totemDefeituosa) {
        this.totemASerAposentado = totemDefeituosa;
    }
}
