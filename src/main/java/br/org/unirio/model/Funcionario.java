package br.org.unirio.model;

import java.util.UUID;

public class Funcionario {
    private UUID id;
    private String nome;
    private long matricula;
    private boolean indicadorBr;
    private Documento documento;

    public Funcionario(){
        super();
    }

    public Funcionario(UUID id, String nome, long matricula, boolean indicadorBr, Documento documento) {
        this.id = id;
        this.nome = nome;
        this.matricula = matricula;
        this.indicadorBr = indicadorBr;
        this.documento = documento;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getMatricula() {
        return matricula;
    }

    public void setMatricula(long matricula) {
        this.matricula = matricula;
    }

    public boolean isIndicadorBr() {
        return indicadorBr;
    }

    public void setIndicadorBr(boolean indicadorBr) {
        this.indicadorBr = indicadorBr;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }
}
