package br.org.unirio.model;

public class PedidoConsertoBicicleta {
    private Funcionario funcionarioRequisisante;
    private Bicicleta bicicletaDefeituosa;

    public PedidoConsertoBicicleta(Bicicleta bicicleta, Funcionario funcionario) {
        this.bicicletaDefeituosa = bicicleta;
        this.funcionarioRequisisante = funcionario;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisisante;
    }

    public void setFuncionarioRequisisante(Funcionario funcionarioRequisisante) {
        this.funcionarioRequisisante = funcionarioRequisisante;
    }

    public Bicicleta getBicicletaDefeituosa() {
        return bicicletaDefeituosa;
    }

    public void setBicicletaDefeituosa(Bicicleta bicicletaDefeituosa) {
        this.bicicletaDefeituosa = bicicletaDefeituosa;
    }
}
