package br.org.unirio.model;

public class PedidoAposentaBicicleta {
    private Bicicleta bicicletaASerAposentada;

    private Funcionario funcionarioRequisitante;

    public PedidoAposentaBicicleta(Bicicleta bicicleta, Funcionario funcionario) {
        this.bicicletaASerAposentada = bicicleta;
        this.funcionarioRequisitante = funcionario;
    }

    public PedidoAposentaBicicleta(){}

    public Bicicleta getBicicletaASerAposentada() {
        return bicicletaASerAposentada;
    }

    public void setBicicletaASerAposentada(Bicicleta bicicletaASerAposentada) {
        this.bicicletaASerAposentada = bicicletaASerAposentada;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisitante;
    }

    public void setFuncionarioRequisitante(Funcionario funcionarioRequisitante) {
        this.funcionarioRequisitante = funcionarioRequisitante;
    }
}
