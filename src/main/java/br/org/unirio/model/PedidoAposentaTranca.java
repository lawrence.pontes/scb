package br.org.unirio.model;

public class PedidoAposentaTranca {
    private Funcionario funcionarioRequisisante;
    private Tranca trancaASerAposentada;

    public PedidoAposentaTranca(Tranca tranca, Funcionario funcionario) {
        this.trancaASerAposentada = tranca;
        this.funcionarioRequisisante = funcionario;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisisante;
    }

    public void setFuncionarioRequisisante(Funcionario funcionarioRequisisante) {
        this.funcionarioRequisisante = funcionarioRequisisante;
    }

    public Tranca getTrancaASerAposentada() {
        return trancaASerAposentada;
    }

    public void setTrancaASerAposentada(Tranca trancaDefeituosa) {
        this.trancaASerAposentada = trancaDefeituosa;
    }
}
