package br.org.unirio.model;

public class PedidoConsertoTranca {
    private Funcionario funcionarioRequisisante;
    private Tranca trancaDefeituosa;

    public PedidoConsertoTranca(Tranca tranca, Funcionario funcionario) {
        this.trancaDefeituosa = tranca;
        this.funcionarioRequisisante = funcionario;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisisante;
    }

    public void setFuncionarioRequisisante(Funcionario funcionarioRequisisante) {
        this.funcionarioRequisisante = funcionarioRequisisante;
    }

    public Tranca getTrancaDefeituosa() {
        return trancaDefeituosa;
    }

    public void setTrancaDefeituosa(Tranca trancaDefeituosa) {
        this.trancaDefeituosa = trancaDefeituosa;
    }
}
