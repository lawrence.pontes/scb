package br.org.unirio.model;

public enum TipoDocumento {
    PASSAPORTE("PASSAPORTE"),
    CPF("CPF"),
    RG("RG"),
    CNH("CNH"),
    CARTEIRA_TRABALHO("CARTEIRA DE TRABALHO");

    private String chave;

    private TipoDocumento(String chave) {
        this.chave = chave;
    }

    public static TipoDocumento from(String chave){
        for(TipoDocumento eGrupo : values()){
            if(eGrupo.name().equalsIgnoreCase(chave))
                return eGrupo;
        }

        return null;
    }
}
