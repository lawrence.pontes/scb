package br.org.unirio.model;

public class PedidoConsertoTotem {
    private Funcionario funcionarioRequisisante;
    private Totem totemDefeituoso;

    public PedidoConsertoTotem(Totem totem, Funcionario funcionario) {
        this.totemDefeituoso = totem;
        this.funcionarioRequisisante = funcionario;
    }

    public Funcionario getFuncionarioRequisitante() {
        return funcionarioRequisisante;
    }

    public void setFuncionarioRequisisante(Funcionario funcionarioRequisisante) {
        this.funcionarioRequisisante = funcionarioRequisisante;
    }

    public Totem getTotemDefeituosa() {
        return totemDefeituoso;
    }

    public void setTotemDefeituosa(Totem totemDefeituosa) {
        this.totemDefeituoso = totemDefeituosa;
    }
}
